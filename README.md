# CharmAnalysis

Analysis code for LBNL Charm project

## Contact Details

| Contact Person | Contact E-mail        |
|----------------|-----------------------|
| Miha Muskinja  | miha.muskinja@cern.ch |

### CMake configuration

The repository comes with its own project configuration file
([CMakeLists.txt](CMakeLists.txt)). It sets up the build of all of the
code of the repository against AthDerivation 21.2.63.0.
(Any version.)

To build the release project, just follow the instructions from the
[analysis software tutorial](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialBasicCMake),
performing an out-of-source build.

```
git clone ssh://git@gitlab.cern.ch:7999/mmuskinj/CharmAnalysis.git
asetup 21.2,AthDerivation,21.2.63.0
mkdir build
cd build/
cmake ../CharmAnalysis/
make
```

### CI Configuration

Nothing here yet.

### Docker Configuration

Nothing here yet.

