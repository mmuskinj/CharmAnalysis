# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

#
# input file config
#

testFile = '/afs/cern.ch/work/m/mmuskinj/public/CharmPhysics/AOD/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.recon.AOD.e3601_s3126_r10724/AOD.15259866._002744.pool.root.1'

# Override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [testFile] 

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

#
# conditions config
#

# ConditionsTag
from AthenaCommon.GlobalFlags  import globalflags
from IOVDbSvc.CondDB import conddb
if len(globalflags.ConditionsTag())!=0:
    conddb.setGlobalTag(globalflags.ConditionsTag())

# Conditions Service for reading conditions data in serial and MT Athena
from IOVSvc.IOVSvcConf import CondSvc
svcMgr += CondSvc()

# Conditions data access infrastructure for serial and MT Athena
from IOVSvc.IOVSvcConf import CondInputLoader
athAlgSeq += CondInputLoader("CondInputLoader")

import StoreGate.StoreGateConf as StoreGateConf
svcMgr += StoreGateConf.StoreGateSvc("ConditionStore")

#
# geometry and magnetic field
#

# Detector flags
from AthenaCommon.DetFlags import DetFlags
DetFlags.Forward_setOff()
DetFlags.Calo_setOff()

# Detector geometry and magnetic field
from AtlasGeoModel import SetGeometryVersion
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetupRecoGeometry

# MagneticField Service
import MagFieldServices.SetupField

# AtlasTrackingGeometrySvc: this line is expected to add it to svcMgr
from TrkDetDescrSvc.AtlasTrackingGeometrySvc import AtlasTrackingGeometrySvc

# Particle Property
from AthenaCommon.Resilience import protectedInclude
protectedInclude("PartPropSvc/PartPropSvc.py")
include.block("PartPropSvc/PartPropSvc.py")

#
# analysis algorithms
#

# Set up the systematics loader/handler algorithm:
sysLoader = CfgMgr.CP__SysListLoaderAlg('SysLoaderAlg' )
sysLoader.sigmaRecommended = 0
athAlgSeq += sysLoader

# Include, and then set up the jet analysis algorithm sequence:
from CharmAnalysis.DSelectionSequence import makeDSelectionAnalysisSequence
DSeq = makeDSelectionAnalysisSequence( dataType='mc' )
DSeq.configure( inputName =  { 'tracks' : 'InDetTrackParticles' },
                outputName = { 'tracks' : 'InDetTrackParticles_Out' } )
# Add all algorithms to the job:
for alg in DSeq:
    athAlgSeq += alg

# Optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
