/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef CHARM_ANALYSIS__CHARM_ANALYSIS_LINKDEF_H
#define CHARM_ANALYSIS__CHARM_ANALYSIS_LINKDEF_H

#ifdef ROOTCORE
// MN: includes needed by RootCore but posing problems in ROOT 6.6.1. remove
// protection when ROOT-7879 fixed Local include(s):
#include <CharmAnalysis/DSelectionAlg.h>
#endif // ROOTCORE

#ifdef __CINT__

#pragma link C++ nestedclass;

#endif // __CINT__

#endif // CHARM_ANALYSIS__CHARM_ANALYSIS_LINKDEF_H