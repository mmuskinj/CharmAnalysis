/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Miha Muskinja

//
// includes
//

#include <CharmAnalysis/DSelectionAlg.h>

#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>

//
// method implementations
//

DSelectionAlg ::DSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
      m_trackToVertexIPEstimator("Trk::TrackToVertexIPEstimator/TrackToVertexIPEstimator"),
      m_VKVrtFitter("Trk::TrkVKalVrtFitter")
{
    // declareProperty("propertName", m_propert, "description of property");
}

StatusCode DSelectionAlg ::initialize() {

    m_systematicsList.addHandle(m_tracksHandle);

    ANA_CHECK(m_systematicsList.initialize());

    ANA_CHECK(m_trackToVertexIPEstimator.retrieve());

    ANA_CHECK(m_VKVrtFitter.retrieve());

    return StatusCode::SUCCESS;
}

StatusCode DSelectionAlg ::execute() {
    return m_systematicsList.foreach (
        [&](const CP::SystematicSet &sys) -> StatusCode {
            // Event info
            const xAOD::EventInfo *eventInfo = nullptr;
            ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

            // Tracks
            xAOD::TrackParticleContainer *tracks = nullptr;
            ANA_CHECK(m_tracksHandle.getCopy(tracks, sys));

            // Primary vertex
            const xAOD::VertexContainer *vertices = 0;
            CHECK(evtStore()->retrieve(vertices, "PrimaryVertices"));
            const xAOD::Vertex *primary = vertices->at(0);

            for (xAOD::TrackParticle *track : *tracks) {
                std::unique_ptr<const Trk::ImpactParametersAndSigma> ip(
                    m_trackToVertexIPEstimator->estimate(track, primary));
                std::cout << "ip->IPd0: " << ip->IPd0 << std::endl;
            }

            return StatusCode::SUCCESS;
        });
}
