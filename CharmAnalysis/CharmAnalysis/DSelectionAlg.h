/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

/// @author Miha Muskinja

#ifndef D_SELECTION_ALG_H
#define D_SELECTION_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SelectionHelpers/ISelectionAccessor.h>
#include <SystematicsHandles/SysCopyHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadHandle.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/TrackParticleContainer.h>

#include <TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h>
#include <TrkVKalVrtFitter/ITrkVKalVrtFitter.h>


class DSelectionAlg : public EL::AnaAlgorithm {
  /// \brief the standard constructor
public:
  DSelectionAlg(const std::string &name, ISvcLocator *pSvcLocator);

public:
  StatusCode initialize() override;

public:
  StatusCode execute() override;

  /// \brief the ITrackToVertexIPEstimator tool
private:
  ToolHandle< Trk::ITrackToVertexIPEstimator > m_trackToVertexIPEstimator;

  /// \brief the ITrkVKalVrtFitter tool
private:
  ToolHandle<Trk::ITrkVKalVrtFitter> m_VKVrtFitter;

  /// \brief the systematics list we run
protected:
  CP::SysListHandle m_systematicsList{this};

  /// \brief the track collection we run on
protected:
  CP::SysCopyHandle<xAOD::TrackParticleContainer> m_tracksHandle{
      this, "InDetTracks", "InDetTracksOut", "the track collection to run on"};
};

#endif // JET_SMEARING_ALG_H
